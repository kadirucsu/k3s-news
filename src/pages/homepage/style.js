import { Dimensions } from 'react-native'
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export const style = {
    safeContainer: {
        flex: 1
    },
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    menuCardStyle: {
        width: deviceWidth * 0.28,
        height: deviceWidth * 0.28,
        margin: deviceWidth * 0.02
    },
    flatList: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    logo: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    logoContainer: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuCardContainer: {
        flex: 0.6,
        justifyContent: 'center',
        alignItems: 'center',
    }
};
