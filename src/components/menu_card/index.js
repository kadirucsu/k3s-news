import React from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { style } from './style'
import { Actions } from 'react-native-router-flux'


const goToNewsPage = (category, categoryName) => {
    Actions.newsPage({ apiCategory: category, title: categoryName })
}

const MenuCard = (props) => {
    return (
        <TouchableOpacity
            onPress={
                () => setTimeout(() => {
                    goToNewsPage(props.apiCategory, props.title)
                }, 50)
            }
            style={style.container}>
            <View style={style.imageContainer}>
                <Image
                    style={style.image}
                    source={props.image} />
            </View>
            <View style={style.titleContainer}>
                <Text>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}


export default MenuCard

