import { NEWS } from '../constants/'
import { NEWS_BASE_URL, NEWS_API_KEY } from '../constants/endpoints'
import { Alert } from 'react-native'
import { Actions } from 'react-native-router-flux';
import tr from '../locale/tr.json'


export const fetchNews = (newsCategory, spinnerToggle) => {

    return async (dispatch) => {

        dispatchNews(dispatch,
            await fetch(NEWS_BASE_URL + "country=tr&category=" + newsCategory + "&apiKey=" + NEWS_API_KEY)
                .then(response => response.json()).then((responseJson) => {
                    return responseJson
                }).catch((error) => {
                    console.error(error);
                }), spinnerToggle);
    }
}

const dispatchNews = (dispatch, data, spinnerToggle) => {



    data.articles && data.articles.length > 0
        ? [
            setTimeout(() => {
                spinnerToggle()
            }, 1000),
            dispatch({
                type: NEWS,
                payload: data.articles
            })
        ]
        : Alert.alert(
            tr.others.alert.error,
            tr.others.alert.connection_error_text,
            [
                {
                    text: tr.others.alert.ok,
                    onPress: () => Actions.pop()
                },
            ],
            { cancelable: false },
        );
}



