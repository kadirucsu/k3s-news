import React, { Component } from 'react'
import { ActivityIndicator, View } from 'react-native'
import { style } from './style'

class K3SSpinner extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isVisible: true
        }
    }

    componentWillReceiveProps(props) {
        this.setState({ isVisible: props.isVisible })
    }

    render() {
        return (
            this.state.isVisible
                ?
                <View style={style.container}>
                    <ActivityIndicator size="large" color='white' />
                </View>
                :
                <View />
        )
    }
}

export default K3SSpinner;
