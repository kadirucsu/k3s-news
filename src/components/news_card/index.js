import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { style } from './style'
import { Actions } from 'react-native-router-flux'


const goDetailPage = (data) => {
    Actions.newsDetailPage({ data: data })
}

const NewsCard = (props) => {

    return (
        <TouchableOpacity
            onPress={
                () => goDetailPage(props.data)
            }
            style={style.container}>
            <View style={style.imageContainer}>
                {props.data.urlToImage !== null
                    ? <Image
                        style={style.image}
                        source={{ uri: props.data.urlToImage }} />
                    : <Image
                        style={style.image}
                        source={require('../../assets/noimage.png')} />}

            </View>
            <View style={style.titleContainer}>
                <Text>{props.data.title}</Text>
            </View>
        </TouchableOpacity>
    )
}


export default NewsCard;
