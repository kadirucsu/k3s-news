import { NEWS } from '../constants/reducer_types'


const INITIAL_STATE = {
    news: "",
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case NEWS:
            {
                return { ...state, news: action.payload }
            }

        default:
            return { ...state };
    }
};