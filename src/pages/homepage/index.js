import React, { Component } from 'react'
import { View, FlatList, Image, SafeAreaView } from 'react-native'
import MenuCard from '../../components/menu_card'
import { style } from './style'


const data = [
    {
        id: 0,
        image: require('../../assets/business.png'),
        title: 'İş',
        apiCategory: 'business'
    },
    {
        id: 1,
        image: require('../../assets/health.png'),
        title: 'Sağlık',
        apiCategory: 'health'
    },
    {
        id: 2,
        image: require('../../assets/science.png'),
        title: 'Bilim',
        apiCategory: 'science'
    },
    {
        id: 3,
        image: require('../../assets/sports.png'),
        title: 'Sporlar',
        apiCategory: 'sports'
    },
    {
        id: 4,
        image: require('../../assets/technology.png'),
        title: 'Teknoloji',
        apiCategory: 'technology'
    },
]

class Homepage extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <SafeAreaView style={style.safeContainer}>
                <View style={style.container} >
                    <View style={style.logoContainer}>
                        <Image
                            source={require('../../assets/k3sLogo.png')}
                            style={style.logo} />
                    </View>
                    <View style={style.menuCardContainer}>
                        <FlatList
                            data={data}
                            numColumns={3}
                            scrollEnabled={false}
                            contentContainerStyle={style.flatList}
                            renderItem={({ item }) =>
                                <View style={style.menuCardStyle}>
                                    <MenuCard
                                        image={item.image}
                                        title={item.title}
                                        apiCategory={item.apiCategory} />
                                </View>
                            }
                            keyExtractor={item => item.id} />
                    </View>

                </View>
            </SafeAreaView>

        )
    }
}

export default Homepage;
