import { Dimensions } from 'react-native'
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export const style = {
    safeContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        flex: 1
    },
    imageContainer: {
        width: deviceWidth,
        height: deviceWidth * 0.6,
        backgroundColor: 'black'
    },
    contentContainer: {
        flex: 1
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover'
    },
    sourceName: {
        fontWeight: 'bold'
    },
    publishedAt: {
        marginLeft: 6,
        fontStyle: 'italic'
    },
    description: {
        fontWeight: 'bold',
        fontSize: deviceWidth * 0.05,
        textAlign: 'justify'
    },
    content: {
        fontSize: deviceWidth * 0.038
    },
    topContainer: {
        flexDirection: 'row',
        minHeight: deviceHeight * 0.05,
        width: deviceWidth,
        alignItems: 'center',
        paddingLeft: 4,

    },
    descriptionContainer: {
        marginVertical: 5,
    },

    dataContainer: {
        margin: 10
    },
    bottomContainer: {
        height: deviceHeight * 0.07,
        width: deviceWidth,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8b0000'
    },
    btnText: {
        color: 'white'
    }
};
