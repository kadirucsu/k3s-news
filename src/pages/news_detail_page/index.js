import React from 'react'
import { Text, SafeAreaView, ScrollView, View, Image, TouchableOpacity } from 'react-native'
import { style } from './style'
import tr from '../../locale/tr.json'


const fixContentText = (content) => {
    debugger
    var index = content.indexOf('…');
    var newContent = content.substring(0, index + 1);
    return newContent;
}

const renderDate = (date) => {
    var date = new Date(date)
    return date.toLocaleDateString()
}

const NewsDetailPage = (props) => {
    debugger
    return (
        <SafeAreaView style={style.safeContainer}>
            <View style={style.container}>
                <ScrollView>
                    <View style={style.imageContainer}>
                        <Image style={style.image} source={{ uri: props.data.urlToImage }} />
                    </View>
                    <View style={style.dataContainer}>
                        <View style={style.topContainer}>
                            <Text style={style.sourceName}>{props.data.source.name}</Text>
                            <Text style={style.publishedAt}>{renderDate(props.data.publishedAt)}</Text>
                        </View>
                        <View style={style.descriptionContainer}>
                            <Text style={style.description}>{props.data.description}</Text>
                        </View>
                        <View style={style.contentContainer}>
                            <Text style={style.content}>{fixContentText(props.data.content)}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
            <TouchableOpacity style={style.bottomContainer}>
                <Text style={style.btnText}>{tr.pages.news_detail_page.bottom_button_text}</Text>
            </TouchableOpacity>
            <SafeAreaView style={{ backgroundColor: 'red', flex: 0 }} />
        </SafeAreaView>
    )
}


export default NewsDetailPage;
