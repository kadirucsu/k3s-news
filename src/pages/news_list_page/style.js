import { Dimensions } from 'react-native'
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export const style = {

    safeContainer: {
        flex: 1
    },
    container: {
        flex: 1
    },
    newsCardContainer: {
        width: deviceWidth * 0.9,
        height: deviceWidth * 0.7,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        margin: deviceWidth * 0.02,
        borderRadius: 5
    },
    flatListContainer: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }


};
