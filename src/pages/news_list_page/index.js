import React, { Component } from 'react'
import { View, SafeAreaView, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { fetchNews } from '../../actions'
import { style } from './style'
import NewsCard from '../../components/news_card'
import K3SSpinner from '../../components/k3s_spinner'

class NewsPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            news: [],
            spinnerVisible: true
        }
    }

    componentDidMount() {
        this.props.fetchNews(this.props.apiCategory, this.toggleSpinner)
    }

    componentWillReceiveProps(props) {
        debugger
        this.setState({ news: props.news })
    }

    toggleSpinner = () => {
        this.setState({ spinnerVisible: false })
    }

    render() {
        return (
            <SafeAreaView style={style.safeContainer}>
                <View style={style.container}>
                    <FlatList
                        keyExtractor={(item, index) => 'key' + index}
                        contentContainerStyle={style.flatListContainer}
                        data={this.state.news}
                        renderItem={({ item }) =>
                            <View style={style.newsCardContainer}>
                                <NewsCard
                                    data={item}
                                />
                            </View>
                        }
                    />
                </View>
                <K3SSpinner isVisible={this.state.spinnerVisible} />
            </SafeAreaView>

        )
    }
}


function bindAction(dispatch) {
    return {
        fetchNews: (apiCategory, toggleSpinner) => dispatch(fetchNews(apiCategory, toggleSpinner))
    };
}

function mapStateToProps(state) {
    const { news } = state.news
    return { news };
}


export default connect(
    mapStateToProps,
    bindAction
)(NewsPage);

