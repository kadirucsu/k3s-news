import { Dimensions } from 'react-native'
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export const style = {

    container: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    imageContainer: {
        flex: 0.75,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f8f8f8'
    },
    titleContainer: {
        flex: 0.25,
        justifyContent: 'center',
        margin: deviceWidth * 0.02
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover'
    }
};
