import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Homepage from './pages/homepage'
import NewsPage from './pages/news_list_page'
import NewsDetailPage from './pages/news_detail_page'


const Routes = () => (
   <Router>
      <Scene key="root">
         <Scene key="homePage" component={Homepage} title="Anasayfa" initial={true} hideNavBar={true} />
         <Scene key="newsPage" component={NewsPage} title="newsPage" hideNavBar={false} />
         <Scene key="newsDetailPage" component={NewsDetailPage} title="newsDetailPage" hideNavBar={false} />
      </Scene>
   </Router>
)

export default Routes
