/**
 * Author CKY
 * year 2019
 * 
 */


import React,{Component}from 'react';
import {AppRegistry} from 'react-native';

import ReduxThunk from 'redux-thunk';
import {Provider} from 'react-redux';
import { createStore , applyMiddleware } from 'redux';
import reducers from './src/reducers';

import Router from './src/router';

export default class App extends Component{

 render(){
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
   return(
       <Provider store={store}>
          <Router/>
      </Provider>
      
   );
 }
}

AppRegistry.registerComponent('App',()=>App);
